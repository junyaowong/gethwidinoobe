# GetHWIDinOOBE



## Getting started

This script will be run in the OOBE process to collect hardware ID/Hash without signing in. 


## How-to
This part will be done in your device to put the script in USB
1. Pull the script from this repository. Either use git clone or download as zip.
2. Copy the script and paste it in your USB

Boot up the Windows device that you want to get the hash.
1. Plug the USB that contains the script.
2. Press Shift + F10 to launch the terminal/CMD
3. Type diskpart > list volume(This command will list down all the hard drive. Look for the USB that contains the script) > select volume X(X = your USB volume name) > assign letter=r > exit > r: > GetAutoPilot.CMD(This will run the cmd)
4. Once it's done, it will auto shut down the device.

## Note
You do not need to worry about file duplication. Every time you run the script, it will add the hash to a new row in the csv.


